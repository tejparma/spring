package p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {
	public static void main(String[] args) {
		
		Arithmetic ob;
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		ob = (Arithmetic) context.getBean("arithmetic");
		try {
			
			System.out.println(ob.add(1, 1));
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}
}

package p1;

import java.util.ArrayList;

public class CityList {
	ArrayList<String> list;

	public CityList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CityList(ArrayList<String> list) {
		super();
		this.list = list;
	}

	public ArrayList<String> getList() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "CityList [list=" + list + "]";
	}
	
}
